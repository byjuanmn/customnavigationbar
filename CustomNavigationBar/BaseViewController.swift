//
//  BaseViewController.swift
//  CustomNavigationBar
//
//  Created by Juan Antonio Martin Noguera on 19/05/16.
//  Copyright © 2016 Cloud On Mobile, S.L. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    
    /*
     definir la properties de los elementos que deseamos modificar en los viewcontrollers hijos
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
//        customizeNavBar();
    }
    
    override func viewWillLayoutSubviews() {
        
        super.viewWillLayoutSubviews()
        customizeNavBar()
    }
    
    private func customizeNavBar(){
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // creamos nuestra propia NavBar
        let myNavBar = UINavigationBar(frame: CGRectMake(0,0, CGRectGetWidth(self.view.bounds), 64.0))
        
        // añadimos un item para el nombre del controller
        
        let item = UINavigationItem()
        item.title = "Main"
        
        
        // añadimos un boton de back
        
        let backBtn = UIBarButtonItem(barButtonSystemItem: .Reply,
                                      target: self,
                                      action: #selector(backButtonTapped))
        
        
        item.leftBarButtonItem = backBtn
        
        item.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Add,
                                                  target: self,
                                                  action: #selector(rightButtonAction))
        
        myNavBar.setItems([item], animated: true)
        self.view.addSubview(myNavBar)
        
    }
    
    @objc private func rightButtonAction(){
        print("right button tapped")
        
    }
    
    @objc private func backButtonTapped() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    

}
